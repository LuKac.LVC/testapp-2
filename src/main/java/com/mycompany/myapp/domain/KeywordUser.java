package com.mycompany.myapp.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "keyword_user")
public class KeywordUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String name;
    @Column(columnDefinition = "integer default 1")
    private int count;
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    public KeywordUser() {
    }

    public KeywordUser(String name, int count, User user) {
        this.name = name;
        this.count = count;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
