package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.service.dto.RequirementProfileDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface RequirementProfileRepository extends PagingAndSortingRepository<RequirementProfile,Integer> {

    @Query("select r from RequirementProfile r where r.user.id = :user_id ")
    public Page<RequirementProfile> findAll(Pageable pageable, @Param("user_id") Long user_id);
}
