package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.KeywordUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface KeywordUserRepository extends JpaRepository<KeywordUser,Integer> {
    @Query("select k from KeywordUser k where k.user.id = :user_id ")
    public List<KeywordUser> findAll(@Param("user_id") Long user_id);
    @Query("select k from KeywordUser k where k.name = :name ")
    public List<KeywordUser> findAll(@Param("name") String name);
}
