package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TagRepository extends JpaRepository<Tag,Integer> {
    @Query("select t from Tag t where t.work = :work ")
    public Page<Tag> findAll(@Param("work") Boolean work, Pageable pageable);
    @Query("select t from Tag t where t.work = :work and t.id=:id")
    public Tag findById(@Param("work") Boolean work, @Param("id") Integer id);
}
