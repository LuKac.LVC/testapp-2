package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.RequirementProfile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CandidateRepository extends PagingAndSortingRepository<Candidate, Integer> {
    @Query("select c from Candidate c where c.firstName like %?1%"
        + "or c.lastName like %?1%"
        + "or c.birthYear like %?1%"
        + "or c.personalNumber like %?1%"
        + "or c.gender like %?1%"
        + "or c.region like %?1%"
        + "or c.country like %?1%"
        + "or c.linkedInLink like %?1%"
        + "or c.employer like %?1%"
        + "or c.firstEmployment like %?1%"
        + "or c.managerialPosition like %?1%"
        + "or c.businessAreas like %?1%"
        + "or c.streetAddress like %?1%"
        + "or c.zipCode like %?1%"
        + "or c.city like %?1%"
        + "or c.phoneNumber like %?1%"
        + "or c.emailPrivate like %?1%"
        + "or c.emailWork like %?1%"
    )
    public Page<Candidate> findAll(Pageable pageable, String keyword);
    @Query("select c from Candidate c where c.firstName like %:#{#requirementProfile.firstName}% " +
        "and c.lastName like %:#{#requirementProfile.lastName}% " +
        "and c.birthYear like %:#{#requirementProfile.birthYear}% "+
        "and c.personalNumber like %:#{#requirementProfile.personalNumber}% " +
        "and c.gender like %:#{#requirementProfile.gender}% " +
        "and c.region like %:#{#requirementProfile.region}% " +
        "and c.linkedInLink like %:#{#requirementProfile.linkedInLink}% " +
        "and c.employer like %:#{#requirementProfile.employer}% " +
        "and c.firstEmployment like %:#{#requirementProfile.firstEmployment}% " +
        "and c.managerialPosition like %:#{#requirementProfile.managerialPosition}% " +
        "and c.businessAreas like %:#{#requirementProfile.businessAreas}% " +
        "and c.streetAddress like %:#{#requirementProfile.streetAddress}% " +
        "and c.zipCode like %:#{#requirementProfile.zipCode}% " +
        "and c.city like %:#{#requirementProfile.city}% " +
        "and c.phoneNumber like %:#{#requirementProfile.phoneNumber}% " +
        "and c.emailPrivate like %:#{#requirementProfile.emailPrivate}% " +
        "and c.emailWork like %:#{#requirementProfile.emailWork}%"

    )
    public List<Candidate> findAll(@Param("requirementProfile") RequirementProfile requirementProfile);

}
