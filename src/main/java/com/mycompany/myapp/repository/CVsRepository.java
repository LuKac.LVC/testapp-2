package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.CVs;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CVsRepository extends PagingAndSortingRepository<CVs,Integer> {
}
