
package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.BlockTag;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BlockTagRepository extends PagingAndSortingRepository<BlockTag,Integer> {
}
