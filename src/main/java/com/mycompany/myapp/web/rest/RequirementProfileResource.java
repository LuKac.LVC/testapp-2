package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.repository.RequirementProfileRepository;
import com.mycompany.myapp.security.AuthoritiesConstants;
import com.mycompany.myapp.service.RequirementProfileService;
import com.mycompany.myapp.service.dto.RequirementProfileDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/requirementProfile")
public class RequirementProfileResource {
    @Autowired
    private RequirementProfileService requirementProfileService;
    @PostMapping("/create")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER+ "\")")
    public ResponseEntity<?> createRequirementProfile(@RequestBody RequirementProfile requirementProfile) {
        RequirementProfileDTO result = requirementProfileService.createRequirementProfile(requirementProfile);
        return ResponseEntity.ok(result);
    }

    @PutMapping("/update")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<?> updateRequirementProfile(@RequestBody RequirementProfile requirementProfile){
        RequirementProfileDTO result = requirementProfileService.updateRequirementProfile(requirementProfile);
        return  ResponseEntity.ok(result);
    }

    @DeleteMapping("/delete")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<?> deleteRequirementProfile(@RequestParam(name = "id") Integer id){
        RequirementProfileDTO result = requirementProfileService.deleteRequirementProfile(id);
        return ResponseEntity.ok(result);
    }

    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<?> getAllRequirementProfile(Pageable pageable) {
        List<RequirementProfileDTO> list = requirementProfileService.getAllRequirementProfileSort(pageable);
        return ResponseEntity.ok(list);
    }
}
