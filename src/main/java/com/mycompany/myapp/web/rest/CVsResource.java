package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.CVs;
import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.security.AuthoritiesConstants;
import com.mycompany.myapp.service.CVsService;
import com.mycompany.myapp.service.dto.CVsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/cvs")
public class CVsResource {
    @Autowired
    private CVsService cVsService;
    @PostMapping("/pdf")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> uploadCv(@RequestParam(name = "file", required = true) MultipartFile file, @RequestParam(name="idCandidate") Integer idCandidate) throws IOException {
        CVsDTO result = cVsService.upLoad(file,idCandidate);
        return ResponseEntity.ok(result);
    }
    @PutMapping("/pdf")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> updateCvPdf(@RequestParam(name = "file", required = true) MultipartFile file, @RequestParam(name = "id") Integer id) throws IOException {
        CVsDTO result = cVsService.updateCvPdf(file, id);
        return ResponseEntity.ok(result);
    }

    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> getCvs(@RequestParam("page") Integer page) throws IOException {
        List<CVsDTO> result = cVsService.getAllCvs(page);
        return ResponseEntity.ok(result);
    }
}
