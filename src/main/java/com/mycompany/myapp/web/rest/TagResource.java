package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.KeywordUser;
import com.mycompany.myapp.domain.Tag;
import com.mycompany.myapp.security.AuthoritiesConstants;
import com.mycompany.myapp.service.KeyworduUserService;
import com.mycompany.myapp.service.TagService;
import com.mycompany.myapp.service.dto.KeyWordUserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tag")
public class TagResource {
    @Autowired
    private TagService tagService;
    @Autowired
    private KeyworduUserService keywordUserService;

    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> getAllTag(Pageable pageable){
        Page<Tag> tagList = tagService.getAllTag(pageable);
        return ResponseEntity.ok(tagList.getContent());
    }
    @PostMapping("/create")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> createTag(@RequestBody Tag tag){
        Tag tag1 = tagService.createTag(tag);
        return ResponseEntity.ok(tag1);

    }
    @PutMapping("/update")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> updateTag(@RequestBody Tag tag){
        Tag tag1 = tagService.updateTagByForm(tag);
        return ResponseEntity.ok(tag1);
    }
    @DeleteMapping("/delete")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> deleteTag(@RequestParam("id") Integer id){
        Tag tag1 = tagService.deleteTag(id);
        return ResponseEntity.ok(tag1);

    }
    @PostMapping("/switch")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> switchBlockTagtoTag(@RequestParam("idBlockTag") Integer id){
        Tag tag = tagService.switchBlockTagToTag(id);
        return ResponseEntity.ok(tag);
    }
    @GetMapping("/detail")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> detailTag(@RequestParam("idTag") Integer idTag){
        List<KeyWordUserDTO> list = keywordUserService.getDetailTag(idTag);
        return ResponseEntity.ok(list);
    }
}
