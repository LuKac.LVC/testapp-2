package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.BlockTag;
import com.mycompany.myapp.security.AuthoritiesConstants;
import com.mycompany.myapp.service.BlockTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/blocktag")
public class BlockTagResource {

    @Autowired
    private BlockTagService blockTagService;

    @PostMapping("/create")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> createBlockTag(@RequestBody BlockTag blockTag){
        BlockTag blockTag1 = blockTagService.createBlockTag(blockTag);
        return ResponseEntity.ok(blockTag1);
    }

    @PutMapping("/update")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> updateBlockTag(@RequestBody BlockTag blockTag){
        BlockTag blockTag1 = blockTagService.updateBlockTag(blockTag);
        return ResponseEntity.ok(blockTag1);
    }

    @DeleteMapping("/delete")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> deletBlockTag(@RequestParam("id") Integer id){
        BlockTag blockTag1 = blockTagService.deleteBlockTag(id);
        return ResponseEntity.ok(blockTag1);
    }

    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> getAllBlockTag(@RequestParam("page") Integer page){
        List<BlockTag> list = blockTagService.getAllBlockTag(page).getContent();
        return  ResponseEntity.ok(list);
    }
    @PostMapping("/switch")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<?> switchTagtoBlockTag(@RequestParam("idTag") Integer id){
        BlockTag blockTag = blockTagService.switchTagToBlockTag(id);
        return ResponseEntity.ok(blockTag);
    }
}
