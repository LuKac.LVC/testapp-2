package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.Notification;
import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.repository.NotificationRepository;
import com.mycompany.myapp.service.UserService;
import com.mycompany.myapp.service.dto.NotificationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationService {
    @Autowired
    private NotificationRepository  notificationRepository;
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private UserService userService;

    public NotificationDTO getDTO(Notification notification){
        NotificationDTO notificationDTO = new NotificationDTO();
        notificationDTO.setId(notification.getId());
        notificationDTO.setContent(notification.getContent());
        notificationDTO.setSubject(notification.getSubject());
        notificationDTO.setEmailUser(notification.getUser().getEmail());
        return notificationDTO;
    }
    public NotificationDTO saveNotification(List<Candidate> candidateList,RequirementProfile requirementProfile){
        Notification notification = new Notification();

        notification.setSubject("List candidates matches with Requiment Profile id=" + requirementProfile.getId());
        String content ="";
        for (Candidate candidate: candidateList) {
            content += "\nid: " + candidate.getId()
                + "\nfirstName: " + candidate.getFirstName()
                + "\nlastName: " + candidate.getLastName()
                + "\nbirthYear: " + candidate.getBirthYear()
                + "\npersonalNumber: " + candidate.getPersonalNumber()
                + "\ngender: " + candidate.getGender()
                + "\nregion: " + candidate.getRegion()
                + "\ncountry: " + candidate.getCountry()
                + "\nlinkedInLink: " + candidate.getLinkedInLink()
                + "\nemployer: " + candidate.getEmployer()
                + "\nfirstEmployment: " + candidate.getFirstEmployment()
                + "\nmanagerialPosition: " + candidate.getManagerialPosition()
                + "\nbusinessAreas: " + candidate.getBusinessAreas()
                + "\nstreetAddress: " + candidate.getStreetAddress()
                + "\nzipCode: " + candidate.getZipCode()
                + "\ncity: " + candidate.getCity()
                + "\nphoneNumber: " + candidate.getPhoneNumber()
                + "\nemailPrivate: " + candidate.getEmailPrivate()
                + "\nemailWork: " + candidate.getEmailWork()+"\n\n";
        }
        content += "\nLink: "+"http://localhost:8080/candidate/"+requirementProfile.getId();
        notification.setContent(content);
        notification.setUser(requirementProfile.getUser());
        notificationRepository.save(notification);
        sendEmail(getDTO(notification));
        return getDTO(notification);
    }
    public NotificationDTO saveNotification(List<Candidate> candidateList){
        Notification notification = new Notification();

        notification.setSubject("New candidates");
        String content ="";
        for (Candidate candidate: candidateList) {
            content += "\nid: " + candidate.getId()
                + "\nfirstName: " + candidate.getFirstName()
                + "\nlastName: " + candidate.getLastName()
                + "\nbirthYear: " + candidate.getBirthYear()
                + "\npersonalNumber: " + candidate.getPersonalNumber()
                + "\ngender: " + candidate.getGender()
                + "\nregion: " + candidate.getRegion()
                + "\ncountry: " + candidate.getCountry()
                + "\nlinkedInLink: " + candidate.getLinkedInLink()
                + "\nemployer: " + candidate.getEmployer()
                + "\nfirstEmployment: " + candidate.getFirstEmployment()
                + "\nmanagerialPosition: " + candidate.getManagerialPosition()
                + "\nbusinessAreas: " + candidate.getBusinessAreas()
                + "\nstreetAddress: " + candidate.getStreetAddress()
                + "\nzipCode: " + candidate.getZipCode()
                + "\ncity: " + candidate.getCity()
                + "\nphoneNumber: " + candidate.getPhoneNumber()
                + "\nemailPrivate: " + candidate.getEmailPrivate()
                + "\nemailWork: " + candidate.getEmailWork()+"\n\n";
        }
        notification.setContent(content);
        notification.setUser(userService.getUser());
        notificationRepository.save(notification);
        sendEmail(getDTO(notification));
        return getDTO(notification);
    }
    public void sendEmail(NotificationDTO notificationDTO){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("tcuong554@gmail.com");
        message.setTo(notificationDTO.getEmailUser());
        message.setText(notificationDTO.getContent());
        message.setSubject(notificationDTO.getSubject());
        mailSender.send(message);
    }
}
