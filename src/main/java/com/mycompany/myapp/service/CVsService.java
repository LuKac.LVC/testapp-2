package com.mycompany.myapp.service;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.mycompany.myapp.domain.CVs;
import com.mycompany.myapp.repository.CVsRepository;
import com.mycompany.myapp.repository.CandidateRepository;
import com.mycompany.myapp.service.dto.CVsDTO;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CVsService {
    @Autowired
    private CVsRepository cVsRepository;
    @Autowired
    private UserACL userACL;
    @Autowired
    private CandidateRepository candidateRepository;
    @Autowired
    private TagService tagService;

    public CVsDTO get(CVs cVs){
        CVsDTO cVsDTO = new CVsDTO();
        cVsDTO.setId(cVs.getId());
        cVsDTO.setName(cVs.getName());
        cVsDTO.setContent(cVs.getContent());
        cVsDTO.setIdCandidate(cVs.getCandidate().getId());
        return cVsDTO;
    }
    public CVsDTO upLoad(MultipartFile file, Integer  idCandidate) throws IOException {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        PdfReader reader = new PdfReader(file.getInputStream());
        int n = reader.getNumberOfPages();
        CVs cVs = new CVs();
        String content="";
        for (int i=1;i<=n;i++){
            content += PdfTextExtractor.getTextFromPage(reader,i).trim() + "\n";
        }
        if(content.isEmpty()) {
            throw  new BadRequestException("error.valueEmpty",null);
        }
        cVs.setContent(content);
        cVs.setName(file.getOriginalFilename());
        try {
            cVs.setCandidate(candidateRepository.findById(idCandidate).get());
        } catch (Exception e) {
            throw  new BadRequestException("error.notFound",null);
        }
        tagService.createTagByCVsPdf(splitContent(content));
        CVs cVs1 = cVsRepository.save(cVs);
        return get(cVs1);
    }

    public CVsDTO updateCvPdf(MultipartFile file, Integer  id) throws IOException {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        PdfReader reader = new PdfReader(file.getInputStream());
        int n = reader.getNumberOfPages();
        String content="";
        for (int i=1;i<=n;i++){
            content += PdfTextExtractor.getTextFromPage(reader,i).trim() + "\n";
        }
        if(content.isEmpty()) {
            throw  new BadRequestException("error.valueEmpty",null);
        }
        try {
            CVs cVs = cVsRepository.findById(id).get();
            cVs.setContent(content);
            cVs.setName(file.getOriginalFilename());
            tagService.createTagByCVsPdf(splitContent(content));
            cVsRepository.save(cVs);
            return get(cVs);
        } catch (Exception e) {
            throw  new BadRequestException("error.notFound",null);
        }
    }

    public List<CVsDTO> getAllCvs(Integer page){
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Pageable pageable = PageRequest.of(page, 10);
        List<CVs> cVsList= cVsRepository.findAll(pageable).getContent();
        List<CVsDTO> cVsDTOList = new ArrayList<>();
        for(CVs cVs : cVsList){
            cVsDTOList.add(get(cVs));
        }
        return cVsDTOList;
    }
    public String[] splitContent(String content){
        String regex = "[.,;?'!]";
        String content1 = content.replaceAll(regex," ");
        String[] list = content1.split("\\s+");
        return list;
    }
}
