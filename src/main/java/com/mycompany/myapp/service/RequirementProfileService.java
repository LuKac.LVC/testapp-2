package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.repository.CandidateRepository;
import com.mycompany.myapp.repository.RequirementProfileRepository;
import com.mycompany.myapp.service.dto.RequirementProfileDTO;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RequirementProfileService {
    @Autowired
    private RequirementProfileRepository requirementProfileRepository;

    @Autowired
    private CandidateRepository candidateRepository;
    @Autowired
    private UserACL userACL;
    @Autowired
    private UserService userService;
    @Autowired
    private NotificationService notificationService;

    public RequirementProfileDTO getDTO(RequirementProfile requirementProfile){
        RequirementProfileDTO requirementProfileDTO = new RequirementProfileDTO();
        requirementProfileDTO.setId(requirementProfile.getId());
        requirementProfileDTO.setFirstName(requirementProfile.getFirstName());
        requirementProfileDTO.setLastName(requirementProfile.getLastName());
        requirementProfileDTO.setBirthYear(requirementProfile.getBirthYear());
        requirementProfileDTO.setPersonalNumber(requirementProfile.getPersonalNumber());
        requirementProfileDTO.setGender(requirementProfile.getGender());
        requirementProfileDTO.setRegion(requirementProfile.getRegion());
        requirementProfileDTO.setCountry(requirementProfile.getCountry());
        requirementProfileDTO.setLinkedInLink(requirementProfile.getLinkedInLink());
        requirementProfileDTO.setEmployer(requirementProfile.getEmployer());
        requirementProfileDTO.setFirstEmployment(requirementProfile.getFirstEmployment());
        requirementProfileDTO.setManagerialPosition(requirementProfile.getManagerialPosition());
        requirementProfileDTO.setBusinessAreas(requirementProfile.getBusinessAreas());
        requirementProfileDTO.setStreetAddress(requirementProfile.getStreetAddress());
        requirementProfileDTO.setZipCode(requirementProfile.getZipCode());
        requirementProfileDTO.setCity(requirementProfile.getCity());
        requirementProfileDTO.setPhoneNumber(requirementProfile.getPhoneNumber());
        requirementProfileDTO.setEmailPrivate(requirementProfile.getEmailPrivate());
        requirementProfileDTO.setEmailWork(requirementProfile.getEmailWork());
        requirementProfileDTO.setUserID(requirementProfile.getUser().getId());
        return requirementProfileDTO;
    }
    public List<RequirementProfile>  getAllRequirementProfile(){
        return (List<RequirementProfile>) requirementProfileRepository.findAll();
    }

    public RequirementProfileDTO createRequirementProfile(RequirementProfile requirementProfile){
        if (!userACL.isUser()) {
            throw new AccessForbiddenException("error.notUser");
        }
        requirementProfile.setUser(userService.getUser());
        RequirementProfile requirementProfile1 = requirementProfileRepository.save(convert(requirementProfile));
        notificationService.saveNotification(candidateRepository.findAll(requirementProfile1),requirementProfile1);

        return  getDTO(requirementProfile1);
    }
    public RequirementProfileDTO updateRequirementProfile(RequirementProfile requirementProfile){
        if (!userACL.isUser()) {
            throw new AccessForbiddenException("error.notUser");
        }
        List<RequirementProfile> list = getAllRequirementProfile();
        for (RequirementProfile requirementProfile1: list){
            if(requirementProfile1.getId()== requirementProfile.getId()){
                requirementProfile.setUser(userService.getUser());
                notificationService.saveNotification(candidateRepository.findAll(convert(requirementProfile)),requirementProfile);
                return getDTO(requirementProfileRepository.save(requirementProfile));
            }
        }
        throw new BadRequestException("Not_found",null);
    }


    public RequirementProfileDTO deleteRequirementProfile(Integer id){
        if (!userACL.isUser()) {
            throw new AccessForbiddenException("error.notUser");
        }
        List<RequirementProfile> list = getAllRequirementProfile();
        for (RequirementProfile requirementProfile1: list){
            if(requirementProfile1.getId()== id){
                RequirementProfile requirementProfile = requirementProfile1;
                requirementProfileRepository.deleteById(id);
                return getDTO(requirementProfile);
            }
        }
        throw new BadRequestException("Not_found",null);
    }
    public List<RequirementProfileDTO> getAllRequirementProfileSort(Pageable pageable) {
        if (!userACL.isUser()) {
            throw new AccessForbiddenException("error.notUser");
        }
        List<RequirementProfile> list = requirementProfileRepository.findAll(pageable, userService.getUser().getId()).getContent();
        List<RequirementProfileDTO> result = new ArrayList<>();
        for (RequirementProfile requirementProfile: list){
            result.add(getDTO(requirementProfile));
        }
        return result;
    }
    public RequirementProfile  convert(RequirementProfile requirementProfile){
        if(requirementProfile.getFirstName()==null) requirementProfile.setFirstName("");
        if(requirementProfile.getLastName()==null) requirementProfile.setLastName("");
        if(requirementProfile.getBirthYear()==null) requirementProfile.setBirthYear("");
        if(requirementProfile.getPersonalNumber()==null) requirementProfile.setPersonalNumber("");
        if(requirementProfile.getGender()==null) requirementProfile.setGender("");
        if(requirementProfile.getRegion()==null) requirementProfile.setRegion("");
        if(requirementProfile.getCountry()==null) requirementProfile.setCountry("");
        if(requirementProfile.getLinkedInLink()==null) requirementProfile.setLinkedInLink("");
        if(requirementProfile.getEmployer()==null) requirementProfile.setEmployer("");
        if(requirementProfile.getFirstEmployment()==null) requirementProfile.setFirstEmployment("");
        if(requirementProfile.getManagerialPosition()==null) requirementProfile.setManagerialPosition("");
        if(requirementProfile.getBusinessAreas()==null) requirementProfile.setBusinessAreas("");
        if(requirementProfile.getStreetAddress()==null) requirementProfile.setStreetAddress("");
        if(requirementProfile.getZipCode()==null) requirementProfile.setZipCode("");
        if(requirementProfile.getCity()==null) requirementProfile.setCity("");
        if(requirementProfile.getPhoneNumber()==null) requirementProfile.setPhoneNumber("");
        if(requirementProfile.getEmailPrivate()==null) requirementProfile.setEmailPrivate("");
        if(requirementProfile.getEmailWork()==null) requirementProfile.setEmailWork("");
        return  requirementProfile;
    }
}
