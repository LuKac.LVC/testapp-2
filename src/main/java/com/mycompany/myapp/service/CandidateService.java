package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.repository.CandidateRepository;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.mycompany.myapp.repository.RequirementProfileRepository;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

@Service
public class CandidateService {

    @Autowired
    private CandidateRepository candidateRepository;
    @Autowired
    private  UserACL userACL;
    @Autowired
    private KeyworduUserService keyworduUserService;
    @Autowired
    private NotificationService notificationService;

    @Autowired
    private RequirementProfileRepository requirementProfileRepository;
    /**
     import Candidate manually
     */
    public Candidate importCandidateManually(Candidate candidate) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        if (candidate.getFirstName() == null || candidate.getLastName() == null) {
            throw new BadRequestException("error.nullValue", null);
        }
        if (!candidate.getBirthYear().matches("[12][0-9]{3}")) {
            throw new BadRequestException("error.invalidYear", null);
        }
        if (!candidate.getPersonalNumber().matches("^\\d{3}-\\d{3}-\\d{3}-\\d{3}$")){
            throw new BadRequestException("error.invalidPersonalNumber", null);
        }
        if (!candidate.getPhoneNumber().matches("\\d+")){
            throw new BadRequestException("error.invalidPhone", null);
        }
        if (!candidate.getEmailPrivate().matches("[a-zA-Z0-9]+[._a-zA-Z0-9!#$%&'*+-/=?^_`{|}~]*[a-zA-Z]*@[a-zA-Z0-9]{2,8}.[a-zA-Z.]{2,6}") || !candidate.getEmailWork().matches("[a-zA-Z0-9]+[._a-zA-Z0-9!#$%&'*+-/=?^_`{|}~]*[a-zA-Z]*@[a-zA-Z0-9]{2,8}.[a-zA-Z.]{2,6}")){
            throw new BadRequestException("error.invalidEmail", null);
        }
        candidateRepository.save(candidate);
        return candidate;
    }

    /**
     import Candidate from file excel
     */
    public List<Candidate> importCandidateByExcel(InputStream input) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        List<Candidate> candidateList = new ArrayList<>();
        try {
            Workbook workbook = new XSSFWorkbook(input);
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.iterator();
            rows.next();
            while (rows.hasNext()) {
                Row row = rows.next();
                Candidate candidate = new Candidate();
                if (row.getCell(0).getCellType() == CellType.STRING) {
                    candidate.setFirstName(row.getCell(0).getStringCellValue());
                }
                if (row.getCell(1).getCellType() == CellType.STRING) {
                    candidate.setLastName(row.getCell(1).getStringCellValue());
                }
                if (row.getCell(2).getCellType() == CellType.STRING || row.getCell(2).getCellType()==CellType.NUMERIC) {
                    if(row.getCell(2).getCellType()==CellType.NUMERIC) candidate.setBirthYear(String.valueOf((int)row.getCell(2).getNumericCellValue()));
                    else candidate.setBirthYear(row.getCell(2).getStringCellValue());

                }
                if (row.getCell(3).getCellType() == CellType.STRING) {
                    candidate.setPersonalNumber(row.getCell(3).getStringCellValue());
                }
                if (row.getCell(4).getCellType() == CellType.STRING) {
                    candidate.setGender(row.getCell(4).getStringCellValue());
                }
                if (row.getCell(5).getCellType() == CellType.STRING) {
                    candidate.setRegion(row.getCell(5).getStringCellValue());
                }
                if (row.getCell(6).getCellType() == CellType.STRING) {
                    candidate.setLinkedInLink(row.getCell(6).getStringCellValue());
                }
                if (row.getCell(7).getCellType() == CellType.STRING) {
                    candidate.setEmployer(row.getCell(7).getStringCellValue());
                }
                if (row.getCell(8).getCellType() == CellType.STRING) {
                    candidate.setFirstEmployment(row.getCell(8).getStringCellValue());
                }
                if (row.getCell(9).getCellType() == CellType.STRING) {
                    candidate.setManagerialPosition(row.getCell(9).getStringCellValue());
                }
                if (row.getCell(10).getCellType() == CellType.STRING) {
                    candidate.setBusinessAreas(row.getCell(10).getStringCellValue());
                }
                if (row.getCell(11).getCellType() == CellType.STRING) {
                    candidate.setStreetAddress(row.getCell(11).getStringCellValue());
                }
                if (row.getCell(12).getCellType() == CellType.STRING || row.getCell(12).getCellType()==CellType.NUMERIC) {
                    if(row.getCell(12).getCellType()==CellType.NUMERIC) candidate.setZipCode(String.valueOf((int)row.getCell(12).getNumericCellValue()));
                    else candidate.setZipCode(row.getCell(12).getStringCellValue());
                }
                if (row.getCell(13).getCellType() == CellType.STRING) {
                    candidate.setCity(row.getCell(13).getStringCellValue());
                }
                if (row.getCell(14).getCellType() == CellType.STRING || row.getCell(14).getCellType()==CellType.NUMERIC) {
                    if(row.getCell(14).getCellType()==CellType.NUMERIC) candidate.setPhoneNumber(String.valueOf((int)row.getCell(14).getNumericCellValue()));
                    else candidate.setPhoneNumber(row.getCell(14).getStringCellValue());
                }
                if (row.getCell(15).getCellType() == CellType.STRING) {
                    candidate.setEmailPrivate(row.getCell(15).getStringCellValue());
                }
                if (row.getCell(16).getCellType() == CellType.STRING) {
                    candidate.setEmailWork(row.getCell(16).getStringCellValue());
                }
                this.importCandidateManually(candidate);
                candidateList.add(candidate);
            }
            workbook.close();
            input.close();
            notificationService.saveNotification(candidateList);
            return candidateList;
        } catch (IOException e) {
            throw new BadRequestException("error.uploadExcelWrongFormat", null);
        }
    }

    /**
     * get all Candidate and sort by field,pagination
     */
    public Page<Candidate> getAllCandidateSort(Pageable pageable) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        return candidateRepository.findAll(pageable);
    }
    /**
     * Search all Candidates with keyword related fields
     * @param page
     * @param keyword
     * @return
     */
    public Page<Candidate> findCandidateByKeyword(Integer page,String keyword){

        if (!userACL.isUser()) {
            throw new AccessForbiddenException("error.notUser");
        }else {
            keyworduUserService.saveKeywordUser(keyword);
        }
        Pageable pageable = PageRequest.of(page,10);
        return candidateRepository.findAll(pageable,keyword);
    }

    /**
     *
     * Search for candidates with information about the fields related to the text
     * @param page
     * @param input
     * @return
     */
    public Page<Candidate> findCandidateByText(Integer page,InputStream input) throws IOException {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        String text = new String(input.readAllBytes());
        List<Candidate> list = (List<Candidate>) candidateRepository.findAll();
        List<Candidate> result = new ArrayList<>();
        text=text.toLowerCase();
        for (Candidate candidate: list){
            if(candidate.getFirstName()!=null && !candidate.getFirstName().isEmpty() && (text.contains(candidate.getFirstName().toLowerCase()))
                || (candidate.getLastName()!=null && !candidate.getLastName().isEmpty() && text.contains(candidate.getLastName().toLowerCase()) )
                || ( candidate.getBirthYear()!=null && !candidate.getBirthYear().isEmpty() && text.contains(candidate.getBirthYear().toLowerCase()))
                || (candidate.getPersonalNumber()!=null && !candidate.getPersonalNumber().isEmpty() && text.contains(candidate.getPersonalNumber().toLowerCase()))
                || (candidate.getGender()!=null && !candidate.getGender().isEmpty() && text.contains(candidate.getGender().toLowerCase()))
                || (candidate.getRegion()!=null && !candidate.getRegion().isEmpty() && text.contains(candidate.getRegion().toLowerCase()) )
                || (candidate.getCountry()!=null && !candidate.getCountry().isEmpty() && text.contains(candidate.getCountry().toLowerCase()))
                || (candidate.getLinkedInLink()!=null && !candidate.getLinkedInLink().isEmpty() && text.contains(candidate.getLinkedInLink().toLowerCase()) )
                || (candidate.getEmployer()!=null && !candidate.getEmployer().isEmpty() && text.contains(candidate.getEmployer().toLowerCase()) )
                || (candidate.getFirstEmployment()!=null && !candidate.getFirstEmployment().isEmpty()  && text.contains(candidate.getFirstEmployment().toLowerCase()))
                || (candidate.getManagerialPosition()!=null && !candidate.getManagerialPosition().isEmpty() && text.contains(candidate.getManagerialPosition().toLowerCase()))
                || (candidate.getBusinessAreas()!=null && !candidate.getBusinessAreas().isEmpty() && text.contains(candidate.getBusinessAreas().toLowerCase()) )
                || (candidate.getStreetAddress()!=null && !candidate.getStreetAddress().isEmpty() && text.contains(candidate.getStreetAddress().toLowerCase()))
                || (candidate.getZipCode()!=null && !candidate.getZipCode().isEmpty() && text.contains(candidate.getZipCode().toLowerCase()))
                || (candidate.getCity()!=null && !candidate.getCity().isEmpty() && text.contains(candidate.getCity().toLowerCase()))
                || (candidate.getPhoneNumber()!=null && !candidate.getPhoneNumber().isEmpty() && text.contains(candidate.getPhoneNumber().toLowerCase()))
                || (candidate.getEmailPrivate()!=null && !candidate.getEmailPrivate().isEmpty() && text.contains(candidate.getEmailPrivate().toLowerCase()))
                || (candidate.getEmailWork()!=null && !candidate.getEmailWork().isEmpty() && text.contains(candidate.getEmailWork().toLowerCase()))
            )
            {
                result.add(candidate) ;
            }
        }
        Page<Candidate> page1 = new PageImpl<>(new ArrayList<>());
        if((page+1)*10<=result.size()){
            page1 = new PageImpl<>(result.subList(page*10,(page+1)*10));
            return page1;
        }else {
            page1 = new PageImpl<>(result.subList(page*10,result.size()));
            return page1;
        }
    }
    public Candidate getCandidateById(Integer id){

        return  candidateRepository.findById(id).get();
    }
    public List<Candidate> notification(Integer idRequirementProfile){
        RequirementProfile requirementProfile = requirementProfileRepository.findById(idRequirementProfile).get();
        List<Candidate> candidateList = candidateRepository.findAll(requirementProfile);
        return candidateList;
    }
}
