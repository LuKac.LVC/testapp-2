package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.User;

import javax.persistence.*;

public class NotificationDTO {
    private int id;
    private String subject;
    private String content;
    private String  emailUser;

    public NotificationDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }
}
