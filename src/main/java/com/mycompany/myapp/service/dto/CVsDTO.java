package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.Candidate;

import javax.persistence.*;

public class CVsDTO {


    private int id;
    private String name;
    private String content;
    private int idCandidate;

    public CVsDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getIdCandidate() {
        return idCandidate;
    }

    public void setIdCandidate(int idCandidate) {
        this.idCandidate = idCandidate;
    }
}
