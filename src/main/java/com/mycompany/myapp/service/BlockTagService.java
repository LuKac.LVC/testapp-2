package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.BlockTag;
import com.mycompany.myapp.domain.Tag;
import com.mycompany.myapp.repository.BlockTagRepository;
import com.mycompany.myapp.repository.TagRepository;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class BlockTagService {
    @Autowired
    private BlockTagRepository blockTagRepository;
    @Autowired
    private UserACL userACL;

    @Autowired
    private TagRepository tagRepository;

    public BlockTag createBlockTag(BlockTag blockTag){
        if(!userACL.isAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        if(blockTag.getName()==null || blockTag.getName().isEmpty()){
            throw new BadRequestException("error.nullValue",null);
        }
        return blockTagRepository.save(blockTag);
    }

    public BlockTag updateBlockTag(BlockTag blockTag){
        if(!userACL.isAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        if(blockTag.getName()==null || blockTag.getName().isEmpty()){
            throw new BadRequestException("error.nullValue",null);
        }
        try{
            blockTagRepository.findById(blockTag.getId());
            return blockTagRepository.save(blockTag);
        } catch (Exception e) {
            throw new BadRequestException("error.Notfound", null);
        }
    }

    public BlockTag deleteBlockTag(Integer id){
        if(!userACL.isAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        try{
            BlockTag blockTag= blockTagRepository.findById(id).get();
            blockTagRepository.deleteById(id);
            return blockTag;
        } catch (Exception e) {
            throw new BadRequestException("error.Notfound", null);
        }
    }
    public Page<BlockTag> getAllBlockTag(Integer page){
        if(!userACL.isAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        Pageable pageable = PageRequest.of(page,10);
        return blockTagRepository.findAll(pageable);
    }

    public BlockTag switchTagToBlockTag(Integer id){
        if(!userACL.isAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        try {
            Tag tag = tagRepository.findById(id).get();
            BlockTag blockTag= new BlockTag();
            blockTag.setName(tag.getName());
            return blockTagRepository.save(blockTag);
        } catch (Exception e) {
            throw new BadRequestException("error.Notfound", null);
        }
    }
}
