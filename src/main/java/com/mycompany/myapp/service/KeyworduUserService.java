package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.KeywordUser;
import com.mycompany.myapp.domain.Tag;
import com.mycompany.myapp.repository.KeywordUserRepository;
import com.mycompany.myapp.service.dto.KeyWordUserDTO;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class KeyworduUserService {
    @Autowired
    private KeywordUserRepository keywordUserRepository;
    @Autowired
    private UserACL userACL;
    @Autowired
    private UserService userService;
    @Autowired
    private TagService tagService;

    public KeyWordUserDTO getDTO(KeywordUser keywordUser){
        KeyWordUserDTO keyWordUserDTO = new KeyWordUserDTO();
        keyWordUserDTO.setId(keywordUser.getId());
        keyWordUserDTO.setName(keywordUser.getName());
        keyWordUserDTO.setCount(keywordUser.getCount());
        keyWordUserDTO.setUserID(keywordUser.getId());
        return keyWordUserDTO;
    }
    public void saveKeywordUser(String name) {
        if (userACL.isUser()) {
            int kt = 0;
            List<KeywordUser> list = keywordUserRepository.findAll(userService.getUser().getId());
            for (KeywordUser keywordUser : list) {
                if (keywordUser.getName().equals(name)) {
                    kt = 1;
                    keywordUser.setCount(keywordUser.getCount() + 1);
                    keywordUserRepository.save(keywordUser);
                }
            }
            if (kt == 0) {
                KeywordUser keywordUser = new KeywordUser();
                keywordUser.setName(name);
                keywordUser.setUser(userService.getUser());
                keywordUser.setCount(1);
                keywordUserRepository.save(keywordUser);
            }
            tagService.saveTag(name);
        }
    }
    public List<KeyWordUserDTO> getDetailTag(Integer idTag){
        try{
            Tag tag = tagService.getById(idTag);
            List<KeywordUser> list = keywordUserRepository.findAll(tag.getName());
            List<KeyWordUserDTO> list1 = new ArrayList<>();
            for (KeywordUser keywordUser:list){
                list1.add(getDTO(keywordUser));
            }
            return list1;
        } catch (Exception e) {
            throw new BadRequestException("Not_found",null);
        }

    }
}
