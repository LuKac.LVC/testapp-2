package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.BlockTag;
import com.mycompany.myapp.domain.KeywordUser;
import com.mycompany.myapp.domain.Tag;
import com.mycompany.myapp.repository.BlockTagRepository;
import com.mycompany.myapp.repository.KeywordUserRepository;
import com.mycompany.myapp.repository.TagRepository;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class TagService {
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private KeywordUserRepository keywordUserRepository;
    @Autowired
    private UserACL userACL;
    @Autowired
    private BlockTagRepository blockTagRepository;

    public void saveTag(String name){
        List<Tag> tags = tagRepository.findAll();
        int kt=0;
        for (Tag tag: tags){
            if(tag.getName().equals(name))
            {
                if (tag.isWork()) {
                    tag.setCount(tag.getCount()+1);
                    tagRepository.save(tag);
                }
                kt=1;
                break;
            }
        }
        if(kt==0) {
            Tag tag = new Tag();
            tag.setName(name);
            tagRepository.save(tag);
        }
        this.updateTag();
    }

    public void updateTag(){
        List<Tag> tags = tagRepository.findAll();
        List<KeywordUser> keywordUsers = keywordUserRepository.findAll();
        for (Tag tag: tags) {
            if (!tag.isWork()) {
                int kt = 0;
                for (KeywordUser keywordUser : keywordUsers) {
                    if (keywordUser.getName().equals(tag.getName()) && keywordUser.getCount() >= 5) kt++;
                }
                if (kt >= 5){
                    tag.setWork(true);
                    tagRepository.save(tag);
                }
            }
        }
    }
    public Page<Tag> getAllTag(Pageable pageable){
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        return tagRepository.findAll(true,pageable);
    }
    public Tag createTag(Tag tag){
        if(!userACL.isAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        if(tag.getName().isEmpty()|| tag.getName()==null) {
            throw new BadRequestException("error.nullValue", null);
        }
        tag.setWork(true);
        Tag tag1 = tagRepository.save(tag);
        return tag1;
    }
    public Tag updateTagByForm(Tag tag){
        if(!userACL.isAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        if(tag.getName().isEmpty()|| tag.getName()==null) {
            throw new BadRequestException("error.nullValue", null);
        }
        try{
            Tag tag1 = tagRepository.findById(tag.getId()).get();
            if(!tag1.isWork()) throw new BadRequestException("error.Notfound", null);
            tag.setWork(true);
            tagRepository.save(tag);
            return tag;
        } catch (Exception e) {
            throw new BadRequestException("error.Notfound", null);
        }
    }

    public Tag deleteTag(Integer id){
        if(!userACL.isAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        try{
            Tag tag1= tagRepository.findById(id).get();
            if(!tag1.isWork()) throw new BadRequestException("error.Notfound", null);
            tagRepository.deleteById(id);
            return tag1;
        } catch (Exception e) {
            throw new BadRequestException("error.Notfound", null);
        }
    }
    public Tag switchBlockTagToTag(Integer id){
        if(!userACL.isAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        try {
            BlockTag blockTag = blockTagRepository.findById(id).get();
            Tag tag = new Tag();
            tag.setName(blockTag.getName());
            tag.setWork(true);
            return tagRepository.save(tag);
        } catch (Exception e) {
            throw new BadRequestException("error.Notfound", null);
        }
    }
    public List<Tag> createTagByCVsPdf(String[] s){
        List<Tag> list = new ArrayList<>();
        List<Tag> list1 = (List<Tag>) tagRepository.findAll();
        for (String i: s){
            int kt=0;
            for (Tag j : list1) {
                if(j.getName().toLowerCase().equals(i.toLowerCase())) {
                    kt=1;
                    break;
                }
            }
            if(kt==0){
                Tag tag = new Tag();
                tag.setName(i);
                tag.setWork(true);
                list.add(tagRepository.save(tag));
            }
        }
        return list;
    }
    public Tag getById(Integer id){
        return tagRepository.findById(true,id);
    }
}
