package com.mycompany.myapp.service.mock;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.repository.CandidateRepository;
import com.mycompany.myapp.service.CandidateService;
import com.mycompany.myapp.service.UserACL;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
@ExtendWith(MockitoExtension.class)
public class CandidateServiceMockTest {
    @InjectMocks
    CandidateService candidateService;
    @Mock
    CandidateRepository candidateRepository;
    @Mock
    UserACL userACL;

    /**
     * Import Candidate from non admin permission user
     */
    @Test
    public void testImportCandidateWithNonAdmin(){
        // given
        given(userACL.isAdmin()).willReturn(false);
        Candidate candidate = new Candidate();
        candidate.setLastName("Tran");
        candidate.setBirthYear("2001");
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> candidateService.importCandidateManually(candidate));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }
    /**
     * Import candidate manually with firtName null
     */
    @Test
    public void testImportCandidateFirtNameNull(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        Candidate candidate = new Candidate();
        candidate.setLastName("Tran");
        candidate.setBirthYear("2001");

        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateManually(candidate));
        //then
        assertThat(result.getMessage()).isEqualTo("error.nullValue");
    }
    /**
     * Import candidate manually with LastName null
     */
    @Test
    public void testImportCandidateLastNameNull(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        Candidate candidate = new Candidate();
        candidate.setFirstName("Cuong");
        candidate.setBirthYear("2001");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateManually(candidate));
        //then
        assertThat(result.getMessage()).isEqualTo("error.nullValue");
    }
    /**
     * BirthYear wrong format
     */
    @Test
    public void testImportCandidateBirthYearFormat(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        Candidate candidate = new Candidate();
        candidate.setFirstName("Cuong");
        candidate.setLastName("Tran");
        candidate.setBirthYear("4000");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateManually(candidate));
        //then
        assertThat(result.getMessage()).isEqualTo("error.invalidYear");
    }
    /**
     * PersonalNumber wrong format
     */
    @Test
    public void testImportCandidatePersonalNumberFormat(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        Candidate candidate = new Candidate();
        candidate.setFirstName("Cuong");
        candidate.setLastName("Tran");
        candidate.setBirthYear("2000");
        candidate.setPersonalNumber("111-222");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateManually(candidate));
        //then
        assertThat(result.getMessage()).isEqualTo("error.invalidPersonalNumber");
    }
    /**
     * PhoneNumber wrong format
     */
    @Test
    public void testImportCandidatePhoneNumberFormat(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        Candidate candidate = new Candidate();
        candidate.setFirstName("Cuong");
        candidate.setLastName("Tran");
        candidate.setBirthYear("2000");
        candidate.setPersonalNumber("111-222-333-444");
        candidate.setPhoneNumber("122ddd");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateManually(candidate));
        //then
        assertThat(result.getMessage()).isEqualTo("error.invalidPhone");
    }
    /**
     * Email wrong format
     */
    @Test
    public void testImportCandidateEmailFormat(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        Candidate candidate = new Candidate();
        candidate.setFirstName("Cuong");
        candidate.setLastName("Tran");
        candidate.setBirthYear("2000");
        candidate.setPersonalNumber("111-222-333-444");
        candidate.setPhoneNumber("123456789");
        candidate.setEmailPrivate("cuong");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateManually(candidate));
        //then
        assertThat(result.getMessage()).isEqualTo("error.invalidEmail");
    }
    /**
     * Import Candidate Success
     */
    @Test
    public void testImortCandidateSuccess(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        Candidate candidate = new Candidate();
        candidate.setFirstName("Cuong");
        candidate.setLastName("Tran");
        candidate.setBirthYear("2000");
        candidate.setPersonalNumber("111-222-333-444");
        candidate.setPhoneNumber("123456789");
        candidate.setEmailPrivate("cuong@gmail.com");
        candidate.setEmailWork("cuong@gmail.com");
        //When
        Candidate candidate1 = candidateService.importCandidateManually(candidate);
        //then
        assertThat(candidate1.getFirstName()).isEqualTo("Cuong");
        assertThat(candidate1.getLastName()).isEqualTo("Tran");
        assertThat(candidate1.getBirthYear()).isEqualTo("2000");
        assertThat(candidate1.getPersonalNumber()).isEqualTo("111-222-333-444");
        assertThat(candidate1.getPhoneNumber()).isEqualTo("123456789");
        assertThat(candidate1.getEmailPrivate()).isEqualTo("cuong@gmail.com");
        assertThat(candidate1.getEmailWork()).isEqualTo("cuong@gmail.com");
    }

    /**
     * Import Candidate from excel with firtName null
     */
    @Test
    public void testImportCandidateExcelNullValue() {
        //given
        given(userACL.isAdmin()).willReturn(true);
        File file = new File("src/main/resources/Candidates - nullValue.xlsx");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateByExcel(new FileInputStream(file)));
        //then
        assertThat(result.getMessage()).isEqualTo("error.nullValue");
    }
    /**
     * Import Candidate from excel with Invalid Year
     */
    @Test
    public void testImportCandidateExcelYearFormat() {
        //given
        given(userACL.isAdmin()).willReturn(true);
        File file = new File("src/main/resources/Candidates - invalidYear.xlsx");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateByExcel(new FileInputStream(file)));
        //then
        assertThat(result.getMessage()).isEqualTo("error.invalidYear");
    }
    /**
     * Import Candidate from excel with Invalid PersonalNumber
     */
    @Test
    public void testImportCandidateExcelPersonalNumberFormat() {
        //given
        given(userACL.isAdmin()).willReturn(true);
        File file = new File("src/main/resources/Candidates - invalidPersonalNumber.xlsx");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateByExcel(new FileInputStream(file)));
        //then
        assertThat(result.getMessage()).isEqualTo("error.invalidPersonalNumber");
    }
    /**
     * Import Candidate from excel with Invalid Phone
     */
    @Test
    public void testImportCandidateExcelPhoneFormat() {
        //given
        given(userACL.isAdmin()).willReturn(true);
        File file = new File("src/main/resources/Candidates - invalidPhone.xlsx");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateByExcel(new FileInputStream(file)));
        //then
        assertThat(result.getMessage()).isEqualTo("error.invalidPhone");
    }
    /**
     * Import Candidate from excel with Invalid Email
     */
    @Test
    public void testImportCandidateExcelEmailFormat() {
        //given
        given(userACL.isAdmin()).willReturn(true);
        File file = new File("src/main/resources/Candidates - invalidEmail.xlsx");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateByExcel(new FileInputStream(file)));
        //then
        assertThat(result.getMessage()).isEqualTo("error.invalidEmail");
    }
    /**
     * Import Candidate from excel success
     */
    @Test
    public void testImportCandidateExcelSuccess() throws FileNotFoundException {
        //given
        given(userACL.isAdmin()).willReturn(true);
        File file = new File("src/main/resources/Candidates.xlsx");
        //When
        List<Candidate> list = candidateService.importCandidateByExcel(new FileInputStream(file));
        //then
        assertThat(list.size()).isEqualTo(10);
        assertThat(list.get(0).getFirstName()).isEqualTo("John1");
        assertThat(list.get(0).getLastName()).isEqualTo("Doe");
        assertThat(list.get(0).getBirthYear()).isEqualTo("1998");
    }
    /**
     * get list Candidate paging
     */
    @Test
    public void getAllCadidate() throws FileNotFoundException {
        //given
        given(userACL.isAdmin()).willReturn(true);
        File file = new File("src/main/resources/Candidates.xlsx");
        Sort sort = Sort.by(Sort.Direction.ASC,"id");
        Pageable pageable = PageRequest.of(1,5,sort);
        List<Candidate> list = candidateService.importCandidateByExcel(new FileInputStream(file));
        given(candidateRepository.findAll(pageable)).willReturn(new PageImpl<>(list.subList(5,10),pageable,list.size()));
        Optional<String> field = Optional.of("id");

        //When
        Page<Candidate> page = candidateService.getAllCandidateSort(pageable);
        //then
        assertThat(page.getContent().size()).isEqualTo(5);
        assertThat(page.getContent().get(0).getFirstName()).isEqualTo("John6");
        assertThat(page.getTotalPages()).isEqualTo(2);

    }
}
