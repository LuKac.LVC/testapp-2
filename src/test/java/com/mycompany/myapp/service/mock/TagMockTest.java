package com.mycompany.myapp.service.mock;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.Tag;
import com.mycompany.myapp.repository.KeywordUserRepository;
import com.mycompany.myapp.repository.TagRepository;
import com.mycompany.myapp.service.TagService;
import com.mycompany.myapp.service.UserACL;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class TagMockTest {
    @InjectMocks
    private TagService tagService;
    @Mock
    private TagRepository tagRepository;
    @Mock
    private UserACL userACL;
    @Mock
    private KeywordUserRepository keywordUserRepository;

    /**
     * Create Tag from non admin permission user
     */
    @Test
    public void testCreateTagWithNonAdmin(){
        // given
        given(userACL.isAdmin()).willReturn(false);
        Tag tag = new Tag();
        tag.setName("1998");
        tag.setWork(true);
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> tagService.createTag(tag));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }
    /**
     * Update Tag from non admin permission user
     */
    @Test
    public void testTagWithNonAdmin(){
        // given
        given(userACL.isAdmin()).willReturn(false);
        Tag tag = new Tag();
        tag.setName("1998");
        tag.setWork(true);
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> tagService.updateTagByForm(tag));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }
    /**
     * Delete Tag from non admin permission user
     */
    @Test
    public void testDeleteTagWithNonAdmin(){
        // given
        given(userACL.isAdmin()).willReturn(false);
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> tagService.deleteTag(1));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }
    /**
     * Create Tag success
     */
    @Test
    public void testCreateTagSuccess(){
        // given
        given(userACL.isAdmin()).willReturn(true);
        Tag tag = new Tag();
        tag.setName("cuong");
        tag.setWork(true);
        given(tagRepository.save(any(Tag.class))).willReturn(tag);

        //When
        Tag result = tagService.createTag(tag);
        //
        assertThat(result.getName()).isEqualTo("cuong");
        assertThat(result.isWork()).isEqualTo(true);
    }
    /**
     * Update Tag success
     */
    @Test
    public void testUpdateTagSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        // input
        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("new name");
        tag.setWork(true);

        // Exist recipient list
        Tag tag1 = new Tag();
        tag1.setId(1);
        tag1.setName("old name");
        tag1.setWork(true);

        given(tagRepository.findById(eq(1))).willReturn(Optional.of(tag1));

        // when
        Tag tag2 = tagService.updateTagByForm(tag);

        // then
        assertThat(tag2.getName()).isEqualTo("new name");
    }
    /**
     * Delete Tag success
     */
    @Test
    public void testDeleteTagSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);
        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("1998");
        tag.setWork(true);

        given(tagRepository.findById(eq(1))).willReturn(Optional.of(tag));

        // when
        Tag tag1 = tagService.deleteTag(1);

        // then
        assertThat(tag1.getName()).isEqualTo("1998");
        assertThat(tag1.getId()).isEqualTo(1);
        assertThat(tag1.isWork()).isEqualTo(true);
    }


}
