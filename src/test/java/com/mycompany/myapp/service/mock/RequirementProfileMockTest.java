package com.mycompany.myapp.service.mock;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.CandidateRepository;
import com.mycompany.myapp.repository.RequirementProfileRepository;
import com.mycompany.myapp.service.CandidateService;
import com.mycompany.myapp.service.RequirementProfileService;
import com.mycompany.myapp.service.UserACL;
import com.mycompany.myapp.service.UserService;
import com.mycompany.myapp.service.dto.RequirementProfileDTO;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class RequirementProfileMockTest {

    @InjectMocks
    RequirementProfileService requirementProfileService;
    @Mock
    RequirementProfileRepository requirementProfileRepository;
    @Mock
    CandidateRepository candidateRepository;
    @Mock
    UserACL userACL;
    @Mock
    UserService userService;

    /**
     * Test create RequirementProfile from non user permission user
     */
    @Test
    public void testCreateRequirementProfileWithNonUser(){
        //given
        given(userACL.isUser()).willReturn(false);
        RequirementProfile requirementProfile = new RequirementProfile();
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> requirementProfileService.createRequirementProfile(requirementProfile));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notUser");
    }
    /**
     * Test create RequirementProfile sucess
     */
    @Test
    public void testCreateRequirementProfile(){
        //given
        given(userACL.isUser()).willReturn(true);

        User user = new User();
        user.setId(1L);

        RequirementProfile requirementProfile = new RequirementProfile();
        requirementProfile.setId(1);
        requirementProfile.setFirstName("Cuong");
        requirementProfile.setLastName("Tran");
        requirementProfile.setBirthYear("2001");
        requirementProfile.setUser(user);
        given(userService.getUser()).willReturn(user);
        given(requirementProfileRepository.save(any(RequirementProfile.class))).willReturn(requirementProfile);
        //when
        RequirementProfileDTO result = requirementProfileService.createRequirementProfile(requirementProfile);
        //Then
        assertThat(result.getId()).isEqualTo(1);
        assertThat(result.getFirstName()).isEqualTo("Cuong");
        assertThat(result.getLastName()).isEqualTo("Tran");
        assertThat(result.getBirthYear()).isEqualTo("2001");
        assertThat(result.getId()).isEqualTo(1L);
    }

    /**
     * Test update RequirementProfile from non user permission user
     */
    @Test
    public void testUpdateRequirementProfileWithNonUser(){
        //given
        given(userACL.isUser()).willReturn(false);
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> requirementProfileService.updateRequirementProfile(new RequirementProfile()));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notUser");
    }

    /**
     * Test update RequirementProfile success
     */
    @Test
    public void testUpdateRequirementProfile(){
        //given

        given(userACL.isUser()).willReturn(true);
        User user = new User();
        user.setId(1L);

        RequirementProfile requirementProfile = new RequirementProfile();
        requirementProfile.setId(1);
        requirementProfile.setFirstName("Tran");
        requirementProfile.setLastName("Cuong");
        requirementProfile.setUser(user);
        RequirementProfile requirementProfile1 = new RequirementProfile();
        requirementProfile1.setId(1);
        requirementProfile1.setFirstName("My");
        requirementProfile1.setLastName("Duyen");
        requirementProfile1.setUser(user);
        given(requirementProfileRepository.findAll()).willReturn(Collections.singletonList(requirementProfile));
        given(requirementProfileRepository.save(any(RequirementProfile.class))).willReturn(requirementProfile1);
        // when
        RequirementProfileDTO result =requirementProfileService.updateRequirementProfile(requirementProfile1);


        // then
        assertThat(result.getId()).isEqualTo(1);
        assertThat(result.getFirstName()).isEqualTo("My");
        assertThat(result.getLastName()).isEqualTo("Duyen");
    }

    /**
     * Test delete RequirementProfile from non user permission user
     */
    @Test
    public void testDeleteRequirementProfileWithNonUser(){
        //given
        given(userACL.isUser()).willReturn(false);
        RequirementProfile requirementProfile = new RequirementProfile();
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> requirementProfileService.deleteRequirementProfile(1));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notUser");
    }
    /**
     * Test delete RequirementProfile success
     */
    @Test
    public void testDeleteRequirementProfile(){
        //given
        given(userACL.isUser()).willReturn(true);

        User user = new User();
        user.setId(1L);

        RequirementProfile requirementProfile = new RequirementProfile();
        requirementProfile.setId(1);
        requirementProfile.setFirstName("Tran");
        requirementProfile.setLastName("Cuong");
        requirementProfile.setUser(user);

        List<RequirementProfile> list = new ArrayList<>();

        //given
        given(requirementProfileRepository.findAll()).willReturn(Collections.singletonList(requirementProfile));

        // when
        List<RequirementProfile> list1 = new ArrayList<>();
        RequirementProfileDTO result =requirementProfileService.deleteRequirementProfile(1);


        // then
        assertThat(list1.size()).isEqualTo(0);
        assertThat(result.getId()).isEqualTo(1);
        assertThat(result.getFirstName()).isEqualTo("Tran");
        assertThat(result.getLastName()).isEqualTo("Cuong");
    }
}
