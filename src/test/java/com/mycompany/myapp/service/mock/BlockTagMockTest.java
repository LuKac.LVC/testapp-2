package com.mycompany.myapp.service.mock;

import com.mycompany.myapp.domain.BlockTag;
import com.mycompany.myapp.domain.Tag;
import com.mycompany.myapp.repository.BlockTagRepository;
import com.mycompany.myapp.service.BlockTagService;
import com.mycompany.myapp.service.UserACL;
import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.cglib.core.Block;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class BlockTagMockTest {
    @InjectMocks
    BlockTagService blockTagService;
    @Mock
    BlockTagRepository blockTagRepository;
    @Mock
    UserACL userACL;

    /**
     * Create BlockTag from non admin permission user
     */
    @Test
    public void testCreateBlockTagWithNonAdmin(){
        // given
        given(userACL.isAdmin()).willReturn(false);
        BlockTag blockTag = new BlockTag();
        blockTag.setName("1998");
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> blockTagService.createBlockTag(blockTag));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }
    /**
     * Update block tag from non admin permission user
     */
    @Test
    public void testUpdateBlockTagWithNonAdmin(){
        // given
        given(userACL.isAdmin()).willReturn(false);
        BlockTag blockTag = new BlockTag();
        blockTag.setName("1998");
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> blockTagService.updateBlockTag(blockTag));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }
    /**
     * Delete Block tag from non admin permission user
     */
    @Test
    public void testDeleteBlockTagWithNonAdmin(){
        // given
        given(userACL.isAdmin()).willReturn(false);
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> blockTagService.deleteBlockTag(1));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * Create BlockTag Name null
     */
    @Test
    public void testCreateBlockTagByNameNull(){
        // given
        given(userACL.isAdmin()).willReturn(true);
        BlockTag blockTag = new BlockTag();
        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> blockTagService.createBlockTag(blockTag));

        // then
        assertThat(result.getMessage()).isEqualTo("error.nullValue");
    }
    /**
     * Create block tag success
     */
    @Test
    public void testCreateBlockTagSuccess(){
        // given
        given(userACL.isAdmin()).willReturn(true);
        BlockTag blockTag = new BlockTag();
        blockTag.setName("cuong");
        given(blockTagRepository.save(any(BlockTag.class))).willReturn(blockTag);

        //When
        BlockTag result = blockTagService.createBlockTag(blockTag);
        //
        assertThat(result.getName()).isEqualTo("cuong");

    }
    /**
     * Update block tag success
     */
    @Test
    public void testUpdateBlockTagSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        // input
        BlockTag blockTag = new BlockTag();
        blockTag.setId(1);
        blockTag.setName("new name");

        // Exist blockTag
        BlockTag blockTag1 = new BlockTag();
        blockTag1.setId(1);
        blockTag1.setName("old name");

        given(blockTagRepository.findById(eq(1))).willReturn(Optional.of(blockTag1));
        given(blockTagRepository.save(any(BlockTag.class))).willReturn(blockTag);
        // when
        BlockTag blockTag2 = blockTagService.updateBlockTag(blockTag);

        // then
        assertThat(blockTag2.getName()).isEqualTo("new name");
    }
    /**
     * Delete Block Tag success
     */
    @Test
    public void testDeleteBlockTagSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);
        BlockTag blockTag = new BlockTag();
        blockTag.setId(1);
        blockTag.setName("1998");

        given(blockTagRepository.findById(eq(1))).willReturn(Optional.of(blockTag));

        // when
        BlockTag blockTag1 = blockTagService.deleteBlockTag(1);

        // then
        assertThat(blockTag1.getName()).isEqualTo("1998");
        assertThat(blockTag1.getId()).isEqualTo(1);
    }

}
